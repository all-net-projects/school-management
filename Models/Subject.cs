﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolManagementSystem.Models
{
    public class Subject
    {
        
        public int SubjectId { get; set; }

      
        public string SubjectName { get; set; }

       //navigation property
        public virtual Teacher Teacher { get; set; }
    }
}
