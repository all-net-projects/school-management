﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SchoolManagementSystem.Models
{
    public class Teacher
    {
        
        public int TeacherId { get; set; }


        public string TeacherName { get; set; }

        public int SchoolId { get; set; }
        public virtual School Schools { get; set; }


        public int SubjectId { get; set; }
        //navigation property
        public virtual Subject Subjects { get; set; }

    }
}
