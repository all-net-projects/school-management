﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Models
{
    public class School
    {

        public int SchoolId { get; set; }

       
        public string SchoolName { get; set; }

        public string Address { get; set; }

       //navigation property
        public virtual ICollection<Teacher> Teachers { get; set; }
    }
}
