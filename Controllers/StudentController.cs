﻿using Microsoft.AspNetCore.Mvc;
using SchoolManagementSystem.DataAccess;
using SchoolManagementSystem.Models;
using SchoolManagementSystem.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace SchoolManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {

        private IServices _services;
        public StudentController(IServices services)
        {
            _services = services;
        }

        [HttpPost]
        [Route("AddSchool")]
        public async Task<ActionResult> Post([FromBody] School school)
        {
            try
            {
                int Result = await _services.AddData(school);
                if (Result > 0)
                {
                    return Ok("Data added successfully");
                }
                else
                {
                    return BadRequest("data not inserted");
                }
               
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }



        }

        [HttpGet]
        [Route("GetAllSchool")]
        public async  Task<ActionResult> Get()
        {
            try
            {
                var data = await _services.GetData();
                if (data == null)
                {
                    return BadRequest("Data not found in database");
                }
                else
                {
                    return Ok(data);
                }
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("GetTeachersFromSchool")]
        public async Task<ActionResult> GetTeacher(string schoolName)
        {
            try
            {
                var data = await _services.GetTeachersBySchool(schoolName);
                if (data == null)
                {
                    return BadRequest("Data not found in database");
                }
                else
                {
                    return Ok(data);
                }
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("AddTeacher")]
        public async Task<ActionResult> AddTeacher([FromBody] Teacher teacher)
        {
            try
            {
                int Status = await _services.AddTeacher(teacher);
                if (Status > 0)
                {
                    return Ok("Data inserted successfully");
                }
                else
                {
                    return BadRequest("Error!! Data not inserted in database");
                }
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpPut]
        [Route("UpdateSchool")]
        public async Task<ActionResult> UpdateSchool([FromBody] School school,int id)
        {
            try
            {
                var SchoolData = await _services.UpdateSchool(id, school);
                if (SchoolData > 0)
                {

                    return Ok("Data updated");


                }
                else
                {
                    return NotFound("Error!!Data not updated");
                }
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("DeleteTeacher")]
        public async Task<ActionResult> DeleteData(int id)
        {try
            {
                int Status = await _services.DeleteData(id);
                if (Status > 0)
                {
                    return Ok("Data deleted from table");
                }
                else
                {
                    return BadRequest("Error!!Data not deleted from table");
                }
            }
            catch (Exception ex) { return BadRequest(ex.Message); } 
        }

       // [HttpPost]
        //public ActionResult GetData()
        //{
        //    var 
        //    return null;
        //}
    }
}
