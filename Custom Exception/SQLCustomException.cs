﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.API.Custom_Exception
{
    public class SQLCustomException:Exception
    {
        public SQLCustomException(string message,Exception exception):base(message,exception)
        {

        }
    }
}
