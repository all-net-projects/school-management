﻿using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.Models;
using SchoolManagementSystem.Repository;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.DataAccess
{
    public class SchoolSystemContext:DbContext
    {
        public SchoolSystemContext(DbContextOptions<SchoolSystemContext> options):base(options)
        {

        }

        public DbSet<School> School { get; set; }

        public DbSet<Teacher> Teacher { get; set; }

        public DbSet<Subject> Subject { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
           

            modelbuilder.ApplyConfiguration(new SchoolEntityConfiguration());
            modelbuilder.ApplyConfiguration(new TeacherEntityConfiguration());

            modelbuilder.Entity<Subject>().HasData(
               new Subject
               {
                   SubjectId = 1,
                   SubjectName = "Marathi"
               },
               new Subject
               {
                   SubjectId = 2,
                   SubjectName = "Hindi"
               }
           );


        }
    }
}
