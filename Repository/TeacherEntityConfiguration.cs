﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SchoolManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Repository
{
    public class TeacherEntityConfiguration : IEntityTypeConfiguration<Teacher>
    {
        public void Configure(EntityTypeBuilder<Teacher> builder)
        {
            builder.HasKey(d => d.TeacherId);

            builder.Property(d => d.TeacherId)
                .UseIdentityColumn();

            builder.Property(d => d.TeacherName)
                .HasMaxLength(50)
                .IsRequired();

            builder.HasOne<School>(S=> S.Schools)
                .WithMany(T=>T.Teachers)
                .HasForeignKey(T=>T.SchoolId)
                .IsRequired(true);

            builder.HasOne<Subject>(S=> S.Subjects)
                .WithOne(T=>T.Teacher)
                .IsRequired(true)
                .HasForeignKey<Teacher>(T=>T.SubjectId);
        }
    }
}
