﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using SchoolManagementSystem.API.Custom_Exception;
using SchoolManagementSystem.DataAccess;
using SchoolManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Repository
{
    public class MockSchoolRepository :IRepository
    {
       private readonly SchoolSystemContext _systemContext;

        public MockSchoolRepository(SchoolSystemContext schoolSystem)
        {
            _systemContext = schoolSystem;
        }


        public async Task<int> AddData(School school)
        {
            try
            {
                
                _systemContext.Add(school);
               return await _systemContext.SaveChangesAsync();

               
            }catch(SqlException ex)
            {
                throw new SQLCustomException("Database error.", ex);
            }
        }

        public async Task<int> AddTeacher(Teacher teacher)
        {
            try
            {
                _systemContext.Add(teacher);
                return await _systemContext.SaveChangesAsync();
            }catch(SqlException ex)
            {
                throw new SQLCustomException("Database errpr,can't insert data",ex);
            }
        }

        public async  Task<int> DeleteData(int id)
        {
            try
            {
                var TeacherData = _systemContext.Teacher.FirstOrDefault(T => T.TeacherId == id);
                _systemContext.Remove(TeacherData);
                return await _systemContext.SaveChangesAsync();
            }catch(SqlException ex)
            {
                throw new SQLCustomException("database error", ex);
            }
        }

        public async Task<List<School>> GetData()
        {
            try
            {
                return await _systemContext.School.ToListAsync();
            }catch(SqlException ex)
            {
                throw new SQLCustomException("data not found", ex);
            }
        }

        public async  Task<School> GetDataById(int id)
        {
            try
            {
                return await _systemContext.School.FirstOrDefaultAsync(s => s.SchoolId == id);
            }catch(SqlException ex)
            {
                throw new SQLCustomException("data not found", ex);
            }
        }

        public async Task<List<string>> GetTeachersBySchool(string schoolName)
        {
            try
            {
                return await (from s in _systemContext.School
                              join t in _systemContext.Teacher on s.SchoolId equals t.SchoolId
                              where s.SchoolName == schoolName
                              select t.TeacherName).ToListAsync();
            }catch(SqlException ex)
            {
                throw new SQLCustomException("database error", ex);
            }
        }

        public async Task<int> UpdateSchool(int id,School school)
        {
            try
            {
                var SchoolData = _systemContext.School.FirstOrDefault(s => s.SchoolId == id);
                if (SchoolData!=null)
                {
                    SchoolData.SchoolName = school.SchoolName;
                    SchoolData.Address = school.Address;
                }
                return await _systemContext.SaveChangesAsync();
            }catch(SqlException ex)
            {
                throw new SQLCustomException("databse error", ex);
            }
        }

      
    }
}
