﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SchoolManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Repository
{
    public class SchoolEntityConfiguration : IEntityTypeConfiguration<School>
    {
        public void Configure(EntityTypeBuilder<School> builder)
        {
            builder.HasKey(s=>s.SchoolId);

            builder.Property(s=>s.SchoolId)
                .UseIdentityColumn();

            builder.Property(s => s.SchoolName).HasColumnName("SchoolName").HasMaxLength(50);

            builder.Property(s => s.Address).HasColumnName("Address").HasMaxLength(50);

            builder.HasMany<Teacher>(T => T.Teachers).WithOne(T => T.Schools).HasForeignKey(T => T.SchoolId);
        }

    }
}
