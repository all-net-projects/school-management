﻿using SchoolManagementSystem.Models;
using SchoolManagementSystem.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Services
{
    public class MockSchoolServices : IServices
    {
        private readonly IRepository _repository;
        public MockSchoolServices(IRepository repository)
        {
            _repository = repository;
        }
        public async Task<int> AddData(School school)
        {
           return await _repository.AddData(school);
        }

        public  async Task<int> AddTeacher(Teacher teacher)
        {
            return await _repository.AddTeacher(teacher);
        }

        public async Task<int> DeleteData(int id)
        {
           return await _repository.DeleteData(id);
        }

        public async Task<List<School>> GetData()
        {
            return await _repository.GetData();
        }

        public  async Task<School> GetDataById(int id)
        {
            return await _repository.GetDataById(id);
        }

        public async Task<List<string>> GetTeachersBySchool(string schoolName)
        {
            return await  _repository.GetTeachersBySchool(schoolName);
        }

        public async Task<int> UpdateSchool(int id,School school)
        {
            return await _repository.UpdateSchool(id,school);
        }
    }
}
