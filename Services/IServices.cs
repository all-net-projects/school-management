﻿using SchoolManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolManagementSystem.Services
{
   public interface IServices
    {
        Task<int> AddData(School school);

        Task<List<School>> GetData();

        Task<School> GetDataById(int id);
        Task<int> AddTeacher(Teacher teacher);

        Task<int> UpdateSchool(int id,School school);

        Task<int> DeleteData(int id);

        Task<List<string>> GetTeachersBySchool(string schoolName);


    }
}
